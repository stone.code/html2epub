package epub

import (
	"bytes"
	"compress/flate"
	"encoding/binary"
	"errors"
	"hash/crc32"
	"io"
)

// Writer creates a zip archive which can be used to create ePub files.
type Writer struct {
	w       io.Writer
	files   []entry
	Comment string
}

type entry struct {
	name             string
	method           uint16
	crc              uint32
	compressedSize   uint32
	uncompressedSize uint32
	Perm             uint32
}

type directoryEnd struct {
	diskNbr            uint16 // unused
	dirDiskNbr         uint16 // unused
	dirRecordsThisDisk uint16 // unused
	directoryRecords   uint16
	directorySize      uint32
	directoryOffset    uint32 // relative to file
	commentLen         uint16
	comment            string
}

// NewWriter returns a new zip archive writer.  The archive is written to w to
// files are added.
func NewWriter(w io.Writer) *Writer {
	return &Writer{w, nil, ""}
}

func writeFileHeader(w io.Writer, e *entry) error {
	sig := uint32(0x04034b50 /*fileHeaderSignature*/)
	readerVersion := uint16(10)
	flags := uint16(0)
	var method uint16 = e.method
	modifiedTime := uint16(0)
	modifiedDate := uint16(0)
	var crc uint32 = e.crc
	var compressedSize = e.compressedSize
	var uncompressedSize uint32 = e.uncompressedSize
	filenameLen := uint16(len(e.name))
	extraLen := uint16(0)

	// Write the file header
	write(w, &sig)
	write(w, &readerVersion)
	write(w, &flags)
	write(w, &method)
	write(w, &modifiedTime)
	write(w, &modifiedDate)
	write(w, &crc)
	write(w, &compressedSize)
	write(w, &uncompressedSize)
	write(w, &filenameLen)
	write(w, &extraLen)
	_, err := w.Write([]byte(e.name))
	if err != nil {
		return err
	}
	return nil
}

// DeflateEntry adds a file to the zip archive.  The file will be compressed.
func (w *Writer) DeflateEntry(name string, data []byte) error {
	// We need to first compress the information
	compressedData := bytes.NewBuffer(nil)
	writer, _ := flate.NewWriter(compressedData, 5 /* level */)
	_, err := writer.Write(data)
	if err != nil {
		return err
	}
	err = writer.Close()
	if err != nil {
		return err
	}
	if compressedData.Len() >= len(data) {
		return w.StoreEntry(name, data)
	}

	// Create the entry for this file
	// This data is stored because much of the information is later
	// replicated in the central directory
	crc := crc32.ChecksumIEEE(data)
	entry := entry{name, 8 /*deflate*/, crc,
		uint32(compressedData.Len()), uint32(len(data)), 2175008768}

	// Write the local file header
	err = writeFileHeader(w.w, &entry)
	if err != nil {
		return err
	}

	// Write the file data
	n, err := w.w.Write(compressedData.Bytes())
	if err != nil {
		return err
	}
	if n != compressedData.Len() {
		return errors.New("internal error.")
	}

	// Make a copy of the metadat before returning
	w.files = append(w.files, entry)
	return nil
}

// StoreEntry adds a  file to the zip archive.  The file will not be compressed.
func (w *Writer) StoreEntry(name string, data []byte) error {
	// Create the entry for this file
	// This data is stored because much of the information is later
	// replicated in the central directory
	crc := crc32.ChecksumIEEE(data)
	entry := entry{name, 0 /*store*/, crc,
		uint32(len(data)), uint32(len(data)), 2175008768}

	// Write the local file header
	err := writeFileHeader(w.w, &entry)
	if err != nil {
		return err
	}

	// Write the file data
	n, err := w.w.Write(data)
	if err != nil {
		return err
	}
	if n != len(data) {
		return errors.New("internal error.")
	}

	// Make a copy of the metadat before returning
	w.files = append(w.files, entry)
	return nil
}

// Close closes the zip archive.
func (w *Writer) Close() error {
	const localFileHeaderSize uint32 = 4 + 10 + 12 + 4

	directorySize := uint32(0)
	directoryOffset := uint32(0)
	for i := 0; i < len(w.files); i++ {
		// Write out the directory entry
		n, err := writeDirectoryEntry(w.w, &w.files[i], directoryOffset)
		if err != nil {
			return err
		}
		directorySize = directorySize + n

		// update our position
		directoryOffset = directoryOffset + localFileHeaderSize + uint32(len(w.files[i].name)) + w.files[i].compressedSize
	}

	directoryRecords := uint16(len(w.files))
	d := directoryEnd{0, 0, directoryRecords, directoryRecords, directorySize, directoryOffset, uint16(len(w.Comment)), w.Comment}
	err := writeDirectoryEnd(w.w, &d)
	return err
}

func writeDirectoryEntry(w io.Writer, file *entry, relativeOffset uint32) (uint32, error) {
	const minSize uint32 = 4 + 12 + 12 + 10 + 8

	sig := uint32(0x02014b50)
	creatorVersion := uint16(3)<<8 + 10
	readerVersion := uint16(10)
	flags := uint16(0)
	modifiedTime := uint16(0)
	modifiedDate := uint16(0)
	filenameLen := int16(len(file.name))
	extraLen := uint16(0)
	commentLen := uint16(0)
	diskNbr := uint16(0)
	internalAttr := uint16(0)
	//relativeOffset := uint32(0)

	// Write the file header
	write(w, &sig)
	write(w, &creatorVersion)
	write(w, &readerVersion)
	write(w, &flags)
	write(w, &file.method)
	write(w, &modifiedTime)
	write(w, &modifiedDate)
	write(w, &file.crc)
	write(w, &file.compressedSize)
	write(w, &file.uncompressedSize)
	write(w, &filenameLen)
	write(w, &extraLen)
	write(w, &commentLen)
	write(w, &diskNbr)
	write(w, &internalAttr)
	write(w, &file.Perm)
	write(w, &relativeOffset)
	n, err := w.Write([]byte(file.name))

	return minSize + uint32(n), err
}

func writeDirectoryEnd(w io.Writer, d *directoryEnd) error {
	sig := uint32(0x06054b50 /*directoryEndSignature*/)

	write(w, sig)
	write(w, d.diskNbr)
	write(w, d.dirDiskNbr)
	write(w, d.dirRecordsThisDisk)
	write(w, d.directoryRecords)
	write(w, &d.directorySize)
	write(w, &d.directoryOffset)
	write(w, d.commentLen)
	_, err := w.Write([]byte(d.comment))
	if err != nil {
		return err
	}

	return nil
}

func write(w io.Writer, data interface{}) error {
	err := binary.Write(w, binary.LittleEndian, data)
	return err
}
