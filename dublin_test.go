package epub

import (
	"reflect"
	"testing"
	"time"
)

func TestDublinCore_GetCoverExtension(t *testing.T) {
	cases := []struct {
		in  string
		out string
	}{
		{"image/png", ".png"},
		{"image/jpeg", ".jpg"},
		{"image/tiff", ".tiff"},
	}

	for i, v := range cases {
		dc := DublinCore{CoverMimeType: v.in}
		out, err := dc.GetCoverExtension()
		if err != nil {
			t.Errorf("%d: unexpected error, %s", i, err)
		}
		if v.out != out {
			t.Errorf("%d: got %s, expected %s", i, out, v.out)
		}
	}
}

func TestDublinCore_GetCoverExtension_Fail(t *testing.T) {
	cases := []struct {
		in string
	}{
		{""},
		{"garbage"},
	}

	for i, v := range cases {
		dc := DublinCore{CoverMimeType: v.in}
		out, err := dc.GetCoverExtension()
		if out != "" {
			t.Errorf("%d: unexpected success, %s", i, out)
		}
		if err == nil {
			t.Errorf("%d: unexpected success", i)
		} else {
			t.Logf("%d: Error: %s", i, err)
		}
	}
}

func TestDublinCore_Normalize(t *testing.T) {
	dc := DublinCore{Title: "  title ", Creator: "   "}
	dc.Normalize()
	if dc.Title != "title" {
		t.Errorf("failed to normalize title")
	}
	if dc.Creator != "" {
		t.Errorf("failed to normalize creator")
	}
}

func TestDublinCore_Set(t *testing.T) {
	cases := []struct {
		key, value string
		out        DublinCore
	}{
		{"title", "A Title", DublinCore{Title: "A Title"}},
		{"CREATOR", "dummy", DublinCore{Creator: "dummy"}},
		{"subject", "dummy", DublinCore{Subject: "dummy"}},
		{"Description", "dummy", DublinCore{Description: "dummy"}},
		{"Publisher", "dummy", DublinCore{Publisher: "dummy"}},
		{"Contributor", "dummy", DublinCore{Contributor: []string{"dummy"}}},
		{"date", "2016", DublinCore{Date: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.UTC)}},
		{"Type", "dummy", DublinCore{Type: "dummy"}},
		{"Format", "dummy", DublinCore{Format: "dummy"}},
		{"Identifier", "dummy", DublinCore{Identifier: "dummy"}},
		{"Source", "dummy", DublinCore{Source: "dummy"}},
		{"Language", "dummy", DublinCore{Language: "dummy"}},
		{"Relation", "dummy", DublinCore{Relation: "dummy"}},
		{"Coverage", "dummy", DublinCore{Coverage: "dummy"}},
		{"Rights", "dummy", DublinCore{Rights: "dummy"}},
		{"CoverURL", "dummy", DublinCore{CoverURL: "dummy"}},
		{"CoverMimeType", "dummy", DublinCore{CoverMimeType: "dummy"}},
	}

	for i, v := range cases {
		dc := DublinCore{}
		dc.Set(v.key, v.value)
		if !reflect.DeepEqual(dc, v.out) {
			t.Errorf("%d: failed to properly set key, %s, %s, %v", i, v.key, v.value, dc)
		}
	}

	// Check for fail
	dc := DublinCore{}
	err := dc.Set("na", "value")
	if err == nil {
		t.Errorf("unexpected success")
	} else {
		t.Logf("expected error, %s", err.Error())
	}
}

func TestDublinCore_VerifyIdentifier(t *testing.T) {
	cases := []struct {
		in  string
		out error
	}{
		{"", nil},
		{"817525766-0", nil},
		{"978-1-4757-1077-9", nil},
		{"978-1-4757-1077-8", ErrInvalidIdentifier},
		{"https://example.org/absolute/URI/with/absolute/path/to/resource.txt", nil},
	}

	for i, v := range cases {
		dc := DublinCore{Identifier: v.in}
		err := dc.VerifyIdentifier()
		if err != v.out {
			t.Errorf("%d: got %v, expected %v", i, err, v.out)
		}
	}
}

func TestDublinCore_VerifyLanguage(t *testing.T) {
	cases := []struct {
		in  string
		out error
	}{
		{"", nil},
		{"en", nil},
		{"fr", nil},
		{"x1", ErrInvalidLanguageCode},
		{"eng", nil},
		{"fra", nil},
		{"x12", ErrInvalidLanguageCode},
		{"blahblahblah", ErrInvalidLanguageCode},
		{"blah en blah", ErrInvalidLanguageCode},
		{"en_CA", nil},
		{"fr_CA", nil},
	}

	for i, v := range cases {
		dc := DublinCore{Language: v.in}
		err := dc.VerifyLanguage()
		if err != v.out {
			t.Errorf("%d (%s): got %v, expected %v", i, v.in, err, v.out)
		}
	}
}
func TestDublinCore_VerifyCoverMimeType(t *testing.T) {
	cases := []struct {
		in  string
		out error
	}{
		{"", nil},
		{"image/png", nil},
		{"image/jpeg", nil},
		{"image/tiff", nil},
		{"x1", ErrInvalidCoverMimeType},
		{"text/plain", ErrInvalidCoverMimeType},
	}

	for i, v := range cases {
		dc := DublinCore{CoverMimeType: v.in}
		err := dc.VerifyCoverMimeType()
		if err != v.out {
			t.Errorf("%d (%s): got %v, expected %v", i, v.in, err, v.out)
		}
	}
}
