package epub

import (
	"errors"
	"mime"
	"net/url"
	"regexp"
	"strings"
	"time"
)

// DublinCore contains metadata for cross-domain information resource description
// (see http://dublincore.org/documents/2004/12/20/dces/).  All elements are included,
// even if they are not used in the generation of ePubs.
//
// Additional metadata not found in the Dublin Core is included in this struct.
// The fields CoverURL and CoverMimeType are useful for ePub generation, even if
// not part of Dublin Core.
type DublinCore struct {
	Title       string    // A name given to the resource.
	Creator     string    // An entity primarily responsible for making the content of the resource.
	Subject     string    // A topic of the content of the resource.
	Description string    // An account of the content of the resource.
	Publisher   string    // An entity responsible for making the resource available
	Contributor []string  // An entity responsible for making contributions to the content of the resource.
	Date        time.Time // A date of an event in the lifecycle of the resource.
	Type        string    // The nature or genre of the content of the resource.
	Format      string    // The physical or digital manifestation of the resource.
	Identifier  string    // An unambiguous reference to the resource within a given context.
	Source      string    // A Reference to a resource from which the present resource is derived.
	Language    string    // A language of the intellectual content of the resource.
	Relation    string    // A reference to a related resource.
	Coverage    string    // The extent or scope of the content of the resource.
	Rights      string    // Information about rights held in and over the resource.

	CoverURL      string // Link to an image representing the cover of this resource
	CoverMimeType string // MimeType of the image linked to by CoverURL
}

var (
	// ErrInvalidCoverMimeType indicates that the mimetype for the cover image is valid
	ErrInvalidCoverMimeType = errors.New("ePub: invalid mime type for cover image")
	// ErrInvalidIdentifier indicates that the identifier was not a valid URI, URL, DOI, or ISBN.
	ErrInvalidIdentifier = errors.New("ePub: invalid identifier")
	// ErrInvalidLanguageCode indicates that the language was not a valid language code
	ErrInvalidLanguageCode = errors.New("ePub: invalid language code")
	// ErrInvalidLanguageCode indicates that the language was not a valid language code
	ErrUnrecognizedKey = errors.New("ePub: unrecognized key")

	regexpLangCode = regexp.MustCompile("^([a-z]{2,3})$|^([a-z]{2}[\\-\\_][A-Z]{2})$" /*([a-z]{2}) | ([a-z]{3}) | ([a-z]{2}[\\-\\_][A-Z]{2})"*/)
)

// GetCoverExtension returns the preferred extension associated with the MimeType
// stored in CoverMimeType.
func (d *DublinCore) GetCoverExtension() (string, error) {
	// Prefer the short extensions for these comment image types.
	switch d.CoverMimeType {
	case "image/png":
		return ".png", nil
	case "image/jpeg":
		return ".jpg", nil
	}

	// Defer to the package mime in stdlib.
	if exts, err := mime.ExtensionsByType(d.CoverMimeType); err == nil {
		if len(exts) == 0 {
			return "", ErrInvalidCoverMimeType
		}
		return exts[0], nil
	}

	return "", ErrInvalidCoverMimeType
}

// Normalize strips leading and trailing spaces from the members.  Other cleanup
// operations may be possible in the future.
func (d *DublinCore) Normalize() {
	d.Title = strings.TrimSpace(d.Title)
	d.Creator = strings.TrimSpace(d.Creator)
	d.Subject = strings.TrimSpace(d.Subject)
	d.Description = strings.TrimSpace(d.Description)
	d.Publisher = strings.TrimSpace(d.Publisher)
	for i := range d.Contributor {
		d.Contributor[i] = strings.TrimSpace(d.Contributor[i])
	}
	d.Type = strings.TrimSpace(d.Type)
	d.Format = strings.TrimSpace(d.Format)
	d.Identifier = strings.TrimSpace(d.Identifier)
	d.Source = strings.TrimSpace(d.Source)
	d.Language = strings.TrimSpace(d.Language)
	d.Relation = strings.TrimSpace(d.Relation)
	d.Coverage = strings.TrimSpace(d.Coverage)
	d.Rights = strings.TrimSpace(d.Rights)

	d.CoverURL = strings.TrimSpace(d.CoverURL)
	d.CoverMimeType = strings.TrimSpace(d.CoverMimeType)
}

// Set updates a member by keyvalue.
func (d *DublinCore) Set(key string, value string) error {
	key = strings.ToLower(key)

	switch strings.ToLower(key) {
	case "title":
		d.Title = value
	case "creator":
		d.Creator = value
	case "author":
		d.Creator = value
	case "subject":
		d.Subject = value
	case "description":
		d.Description = value
	case "publisher":
		d.Publisher = value
	case "contributor":
		d.Contributor = append(d.Contributor, value)
	case "date":
		if tmp, err := ParseDateTime(value); err == nil {
			d.Date = tmp
		} else {
			return err
		}
	case "type":
		d.Type = value
	case "format":
		d.Format = value
	case "identifier":
		d.Identifier = value
	case "source":
		d.Source = value
	case "language":
		d.Language = value
	case "relation":
		d.Relation = value
	case "coverage":
		d.Coverage = value
	case "rights":
		d.Rights = value
	case "coverurl":
		d.CoverURL = value
	case "covermimetype":
		d.CoverMimeType = value
	default:
		return ErrUnrecognizedKey
	}

	return nil
}

// VerifyIdentifier returns an error if the Identifier is not a valid URI, URL,
// DOI, or ISBN.  Note that the DublinCore does not require that this list of format
// is exclusive, so this method may return an error for a valid identifier is a
// different identification system is being used.
//
// Errors are not returned for empty identifiers.
func (d *DublinCore) VerifyIdentifier() error {
	if d.Identifier == "" {
		return nil
	}

	if IsValidISBN(d.Identifier) {
		return nil
	}
	if _, err := url.ParseRequestURI(d.Identifier); err == nil {
		return nil
	}
	// TODO:  Add check for valid DOI

	return ErrInvalidIdentifier
}

// VerifyLanguage returns an error if the valuef of Language is not a valid language
// code, as specified by ISO 639-1 or ISO 639-2.  The format of the language code
// is checked, but the actual language is not verified as being currently approved.
//
// Errors are not returned is Language is empty.
func (d *DublinCore) VerifyLanguage() error {
	if d.Language == "" {
		return nil
	}

	if regexpLangCode.MatchString(d.Language) {
		return nil
	}

	return ErrInvalidLanguageCode
}

// VerifyCoverMimeType returns an error if the value of CoverMimeType is not a valid
// mimetype, or if it does not specify an image.  Note, not all readers will support
// all image types, so the cover image may not appear even if no error is returned
// here.
//
// Errors are not returned is CoverMimeType is empty.
func (d *DublinCore) VerifyCoverMimeType() error {
	if d.CoverMimeType == "" {
		return nil
	}

	// Parsing full spec for mimetype here is probably overkill, but for the moment
	// we rely on stdlib.
	mediatype, _, err := mime.ParseMediaType(d.CoverMimeType)
	if err != nil {
		return ErrInvalidCoverMimeType
	}

	if strings.HasPrefix(mediatype, "image/") {
		return nil
	}

	return ErrInvalidCoverMimeType
}
