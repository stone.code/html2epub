package epub

import (
	"bytes"
	"strconv"
	"strings"
)

// NCX specifies the structure of a publication for use with navigational control.
// The NCX supports the creation of a navigation map, which ePub readers can use
// to provide a table of contents for easy navigation.
//
// The NCX specification allows for the creation of a hierarchical structure for
// navigation map.  However, only flat structures can be be created currently.
//
// Currently, the creation of page lists and navigation lists is not supported.
type NCX struct {
	meta      []ncxMeta
	navPoints []ncxNavPoint
}

type ncxMeta struct {
	Name    string
	Content string
}

type ncxNavPoint struct {
	id        string
	label     string
	src       string
	playOrder int
	class     string
}

// NewNCX returns a new, initialized, NCX.
func NewNCX() NCX {
	return NCX{}
}

// AddMeta adds a metadata element to the head of the NCX file.
func (n *NCX) AddMeta(name string, content string) {
	name = strings.TrimSpace(name)
	n.meta = append(n.meta, ncxMeta{name, content})
}

// AddNavPoint inserts an item into the navigation list.
func (n *NCX) AddNavPoint(label string, src string, id string) {
	label = SimplifyCharset(label)
	playOrder := len(n.navPoints) + 1

	if len(id) == 0 {
		id = "nav-point-" + strconv.Itoa(len(n.navPoints)+1)
	}

	n.navPoints = append(n.navPoints, ncxNavPoint{id, label, src, playOrder, ""})
}

// AddNavPoint inserts an item into the navigation list.
func (n *NCX) AddNavPointWithClass(label string, src string, id string, class string) {
	label = SimplifyCharset(label)
	playOrder := len(n.navPoints) + 1
	class = strings.TrimSpace(class)

	if len(id) == 0 {
		id = "nav-point-" + strconv.Itoa(len(n.navPoints)+1)
	}

	n.navPoints = append(n.navPoints, ncxNavPoint{id, label, src, playOrder, class})
}

// Write create and inserts the NCX file into the ePub.
func (n *NCX) Write(z *Writer, opf *OPF) error {
	file := bytes.NewBuffer(nil)

	_, err := file.WriteString(
		`<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE ncx PUBLIC
     "-//NISO//DTD ncx 2005-1//EN"
     "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">
<ncx version="2005-1" xml:lang="en" xmlns="http://www.daisy.org/z3986/2005/ncx/">
<head>
<!-- The following four metadata items are required for all
        NCX documents, including those conforming to the relaxed
        constraints of OPS 2.0 -->
    <meta name="dtb:uid" content="` + opf.Identifier + `"/>
    <meta name="dtb:depth" content="1"/>
    <meta name="dtb:totalPageCount" content="0"/>
    <meta name="dtb:maxPageNumber" content="0"/>
    <!-- Additional meta attributes (if any ) -->
`)
	if err != nil {
		return err
	}
	for _, item := range n.meta {
		_, err = file.WriteString(`    <meta name="` + item.Name + `" content="` + item.Content + "\"/>\n")
	}
	_, err = file.WriteString("</head>\n")

	// Write the docTitle section
	_, err = file.WriteString("<docTitle><text>")
	err = EscapeString(file, opf.Title)
	_, err = file.WriteString("</text></docTitle>\n")

	// Write the docAuthor section
	_, err = file.WriteString("<docAuthor><text>")
	err = EscapeString(file, opf.Creator)
	_, err = file.WriteString("</text></docAuthor>\n")

	// Write the navMap
	_, err = file.WriteString("<navMap>\n")
	for _, item := range n.navPoints {
		_, err = file.WriteString(`<navPoint id="`)
		_, err = file.WriteString(item.id)
		_, err = file.WriteString(`" playOrder="`)
		_, err = file.WriteString(strconv.Itoa(item.playOrder))
		_, err = file.WriteString(`"`)
		if item.class != "" {
			_, err = file.WriteString(` class="`)
			_, err = file.WriteString(item.class)
			_, err = file.WriteString(`"`)
		}
		_, err = file.WriteString(">\n")
		_, err = file.WriteString("<navLabel><text>")
		err = EscapeString(file, item.label)
		_, err = file.WriteString("</text></navLabel>\n")
		_, err = file.WriteString(`<content src="` + item.src + `"/>`)
		_, err = file.WriteString("\n</navPoint>\n")
	}

	_, err = file.WriteString("</navMap>\n</ncx>\n")
	if err != nil {
		return err
	}

	// Now that the buffer has been initialized with the file contents
	err = z.DeflateEntry("index.ncx", file.Bytes())
	if err != nil {
		return err
	}

	// Update the manifest
	opf.AddItem("ncx", "index.ncx", "application/x-dtbncx+xml")

	return nil
}
