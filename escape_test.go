package epub

import (
	"bytes"
	"testing"
)

func TestSimplifyCharset(t *testing.T) {
	cases := []struct {
		in  string
		out string
	}{
		{"Simple text", "Simple text"},
		{"Hello \u2012 there", "Hello - there"},
		{"\u2012\u2013\u2014\u2015", "----"},
		{"\t\nHello there", "  Hello there"},
		{"Hello \u2018\u2019\u201C\u201D there", "Hello ''\"\" there"},
		{"\u2039Hello, there\u203A", "<Hello, there>"},
	}

	for i, v := range cases {
		out := SimplifyCharset(v.in)
		if v.out != out {
			t.Errorf("%d: got %s, expected %s", i, out, v.out)
		}
	}
}

func TestEscapeString(t *testing.T) {
	cases := []struct {
		in  string
		out string
	}{
		{"Simple text", "Simple text"},
		{`ab"cd`, "ab&quot;cd"},
		{`ab'cd`, "ab&apos;cd"},
		{"<>", "&lt;&gt;"},
		{"Some text &", "Some text &amp;"},
		{"& Some text", "&amp; Some text"},
		{"Hello, 世界", "Hello, &#19990;&#30028;"},
	}

	for i, v := range cases {
		buffer := bytes.NewBuffer(nil)
		err := EscapeString(buffer, v.in)
		if err != nil {
			t.Errorf("%d: unexpected error, %s", err)
		}
		out := string(buffer.Bytes())
		if v.out != out {
			t.Errorf("%d: got %s, expected %s", i, out, v.out)
		}
	}
}
