package epub

import (
	"strings"
)

// IsValidISBN returns true if the string is a valid International Standard Book
// Number (ISBN).  Bth 10-digit and 13-digit ISBNs are accepted.
func IsValidISBN(src string) bool {
	src = normalizeISBN(src)
	if len(src) == 10 {
		return isValidISBN10(src)
	}
	if len(src) == 13 {
		return isValidISBN13(src)
	}
	return false
}

// IsValidISBN returns true if the string is a valid International Standard Book
// Number (ISBN).  Only 10-digit ISBNs are accepted.
func IsValidISBN10(src string) bool {
	src = normalizeISBN(src)
	if len(src) != 10 {
		return false
	}

	return isValidISBN10(src)
}

// IsValidISBN returns true if the string is a valid International Standard Book
// Number (ISBN).  Only 13-digit ISBNs are accepted.
func IsValidISBN13(src string) bool {
	src = normalizeISBN(src)
	if len(src) != 13 {
		return false
	}

	return isValidISBN13(src)
}

func isValidISBN10(src string) bool {
	sum := 0

	for i, v := range src {
		digit := 0
		if i == 9 && (v == 'x' || v == 'X') {
			digit = 10
		} else if v < '0' || v > '9' {
			return false
		} else {
			digit = int(v) - '0'
		}

		factor := 10 - i
		sum = sum + (factor * digit)
	}

	return sum%11 == 0
}

func isValidISBN13(src string) bool {
	sum := 0

	for i, v := range src {
		if v < '0' || v > '9' {
			return false
		}

		digit := int(v) - '0'
		factor := 1 + (i&1)*2
		sum = sum + (factor * digit)
	}
	return sum%10 == 0
}

func normalizeISBN(src string) string {
	return strings.Replace(src, "-", "", -1)
}
