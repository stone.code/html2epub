package epub

import (
	"bytes"
	"path/filepath"
)

const (
	html_open = `<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
`
	html_close = `</html>`
)

// CreateMakefile adds a small makefile to the zip archive for the ePub.  This makefile
// can be used to rebuild the ePub if the contents are unzipped.  The makefile is
// added to the OPF manifest.
//
// The parameter filename should contain the name for the final epub.
func CreateMakefile(z *Writer, opf *OPF, filename string) error {
	err := z.DeflateEntry("makefile", []byte(
		"EPUBNAME = \"../"+filepath.Base(filename)+"\"\n\n"+
			`all:
	rm -f $(EPUBNAME)
	zip -q0X $(EPUBNAME) mimetype
	zip -qXr9D $(EPUBNAME) *
`))
	if err != nil {
		return err
	}

	// Add the makefile to the manifest
	opf.AddItem("makefile", "makefile", "text/plain")
	return nil
}

// CreateMimeTypeFile adds the file 'mimetype' to the zip archive for the ePub.
// This file is required by the standard, and should be the first file added to
// the archive.
func CreateMimeTypeFile(z *Writer) error {
	err := z.StoreEntry("mimetype", []byte("application/epub+zip"))
	return err
}

// CreateMetaInf adds the file 'META-INF/container.xml' to the zip archive for the
// ePub.  This file is required by the standard.
func CreateMetaInf(z *Writer) error {
	err := z.StoreEntry("META-INF/container.xml", []byte(
		`<?xml version="1.0" encoding="UTF-8" ?>
<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
<rootfiles>
<rootfile full-path="index.opf" media-type="application/oebps-package+xml"/>
</rootfiles>
</container>
`))
	return err
}

// CreateTitlePage creates an XHTML file to serve as a title page for the ePub,
// which is inserted into the zip archive along with appropriate entries in the OPF
// manifest and the NCX guide.  Details (such as title, creator, etc.) for the title
// page are obtained from the Dublin Core metadata embedded in the OPF.
func CreateTitlePage(z *Writer, opf *OPF, ncx *NCX) error {
	// Create the XHTML file that will be the title page
	file := bytes.NewBuffer(nil)
	_, err := file.WriteString(html_open)
	if err != nil {
		return err
	}
	_, err = file.WriteString("<head><title>")
	err = EscapeString(file, opf.Title)
	_, err = file.WriteString("</title></head>\n<body>")
	_, err = file.WriteString(`<div class="title-page"><h1 class="title">`)
	err = EscapeString(file, opf.Title)
	_, err = file.WriteString("</h1>\n")
	if len(opf.Creator) > 0 {
		_, err = file.WriteString("<p class=\"creator\">")
		err = EscapeString(file, opf.Creator)
		_, err = file.WriteString("</p>\n")
	}
	if !opf.Date.IsZero() {
		_, err = file.WriteString("<p>")
		_, err = file.WriteString(opf.Date.Format("2006"))
		_, err = file.WriteString("</p>\n")
	}
	_, err = file.WriteString("</div></body>" + html_close)
	if err != nil {
		return err
	}

	// Compress and store the page in the zip archive
	err = z.DeflateEntry("title.xhtml", file.Bytes())
	if err != nil {
		return err
	}

	// Update the manifest, spine, and navigation map.
	opf.AddItem("title", "title.xhtml", "application/xhtml+xml")
	opf.AddItemRef("title", true)
	opf.AddReference(GuideTitlePage, "Title Page", "title.xhtml")
	ncx.AddNavPoint("Title Page", "title.xhtml", "title")

	return nil
}

// Loader provides a callback to load a possibly relative URL or file path.
type Loader interface {
	Load(url string) ([]byte, error)
}

// CreateCover creates an XHTML file to serve as a cover page for the ePub,
// which is inserted into the zip archive along with appropriate entries in the OPF
// manifest and the NCX guide.  The cover page will use the image linked to with the
// CoverURL and CoverMimeType metadata in the OPF.
//
// If the cover image cannot be generated, code will fallback to creating a title
// page.
func CreateCover(z *Writer, opf *OPF, ncx *NCX, loadImage Loader) error {
	if opf.CoverURL == "" {
		return CreateTitlePage(z, opf, ncx)
	}

	// Create the image file with the cover
	imageData, err := loadImage.Load(opf.CoverURL)
	if err != nil {
		return err
	}
	image_filename, err := opf.GetCoverExtension()
	if err != nil {
		return err
	}
	image_filename = "cover" + image_filename
	err = z.DeflateEntry(image_filename, imageData)
	if err != nil {
		return err
	}
	opf.AddItem("cover-image", image_filename, opf.CoverMimeType)
	ncx.AddMeta("cover", "cover-image")

	// Create the XHTML file that will display the cover
	buffer := bytes.NewBuffer(nil)
	_, err = buffer.WriteString(html_open)
	if err != nil {
		return err
	}
	_, err = buffer.WriteString("<head><title>")
	err = EscapeString(buffer, opf.Title)
	_, err = buffer.WriteString("</title></head>\n<body>")
	_, err = buffer.WriteString(`<div id="cover-image">`)
	_, err = buffer.WriteString(`<img src="` + image_filename + `" style="max-width:100%" alt="`)
	err = EscapeString(buffer, opf.Title)
	_, err = buffer.WriteString(`"/></div>`)
	_, err = buffer.WriteString("</body>" + html_close)
	if err != nil {
		return err
	}

	// Compress and store the page in the zip archive
	err = z.DeflateEntry("cover.xhtml", buffer.Bytes())
	if err != nil {
		return err
	}

	// Update the manifest, spine, and navigation map.
	opf.AddItem("cover", "cover.xhtml", "application/xhtml+xml")
	opf.AddItemRef("cover", true)
	opf.AddReference(GuideCover, "Cover Page", "cover.xhtml")
	ncx.AddNavPoint("Cover Page", "cover.xhtml", "cover")
	return nil
}

// CreateSection will create a file in the zip archive for the ePub.  The parameter
// data should contain a valid XHTML file.
func CreateSection(z *Writer, opf *OPF, ncx *NCX, id, filename, title string, data []byte) error {
	err := z.DeflateEntry(filename, data)
	if err != nil {
		return err
	}

	opf.AddItem(id, filename, "application/xhtml+xml")
	opf.AddItemRef(id, true)
	opf.AddReference(GuideText, title, filename)
	ncx.AddNavPoint(title, filename, id)
	return nil
}
