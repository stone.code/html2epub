package epub

import (
	"errors"
	"time"
)

var (
	ErrInvalidDateFormat = errors.New("unrecognized date format")
)

// ParseDateTime parses a string that is in HTML format (see http://www.w3.org/TR/NOTE-datetime).
func ParseDateTime(date string) (time.Time, error) {
	// See https://www.w3.org/TR/NOTE-datetime

	ret, err := time.Parse("2006", date)
	if err == nil {
		return ret, nil
	}
	ret, err = time.Parse("2006-01", date)
	if err == nil {
		return ret, nil
	}
	ret, err = time.Parse("2006-01-02", date)
	if err == nil {
		return ret, nil
	}
	ret, err = time.Parse("2006-01-02T15:04-07:00", date)
	if err == nil {
		return ret, nil
	}
	ret, err = time.Parse("2006-01-02T15:04:05-07:00", date)
	if err == nil {
		return ret, nil
	}
	ret, err = time.Parse("2006-01-02T15:04:05.0-07:00", date)
	if err == nil {
		return ret, nil
	}
	return time.Time{}, ErrInvalidDateFormat
}
