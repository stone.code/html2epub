package epub

import (
	"bytes"
	"errors"
	"mime"
	"path"
	"strings"
)

var (
	// Metadata required by the ePub standard is missing.
	ErrIncompleteMetadata = errors.New("epub: incomplete metadata")
)

// OPF contains data for the Open Packaging Format (see http://www.idpf.org/epub/20/spec/OPF_2.0.1_draft.htm).
type OPF struct {
	DublinCore
	items      []opfItem
	itemrefs   []opfItemRef
	references []opfReference
}

type opfItem struct {
	id        string
	href      string
	mediaType string
	fallback  string
}

type opfItemRef struct {
	idRef    string
	isLinear bool
}

// GuideType enumerates the types of reference that can be added to the guide.
type GuideType int

const (
	GuideCover = GuideType(iota)
	GuideTitlePage
	GuideTOC
	GuideIndex
	GuideGlossary
	GuideAcknowledgements
	GuideBibliography
	GuideColophon
	GuideCopyrightPage
	GuideDedication
	GuideEpigraph
	GuideForeword
	GuideLOI // 	list of illustrations
	GuideLOT // 	list of tables
	GuideNotes
	GuidePreface
	GuideText
)

func (g GuideType) String() string {
	switch g {
	case GuideCover:
		return "cover"
	case GuideTitlePage:
		return "title-page"
	case GuideTOC:
		return "toc"
	case GuideIndex:
		return "index"
	case GuideGlossary:
		return "glossary"
	case GuideAcknowledgements:
		return "acknowledgements"
	case GuideBibliography:
		return "bibliography"
	case GuideColophon:
		return "colophon"
	case GuideCopyrightPage:
		return "copyright-page"
	case GuideDedication:
		return "dedication"
	case GuideEpigraph:
		return "epigraph"
	case GuideForeword:
		return "foreword"
	case GuideLOI:
		return "loi"
	case GuideLOT:
		return "lot"
	case GuideNotes:
		return "notes"
	case GuidePreface:
		return "preface"
	case GuideText:
		return "text"
	}
	panic(nil)
}

type opfReference struct {
	Type  GuideType
	Title string
	Href  string
}

// NewOPF returns a new, initialized OPF.
func NewOPF() OPF {
	return OPF{}
}

// NewOPFWithDublinCore returns a new, initialized OPF.  Metadata for the ePub is
// copied from the Dublin Core data provided.
func NewOPFWithDublinCore(dc *DublinCore) OPF {
	return OPF{DublinCore: *dc}
}

// AddItem records a file that has been placed in the zip archive for the ePub
// with the OPF manifset.
func (o *OPF) AddItem(id string, href string, mediaType string) *opfItem {
	id = strings.TrimSpace(id)
	href = strings.TrimSpace(href)
	mediaType = strings.TrimSpace(mediaType)

	if mediaType == "" {
		mediaType = mime.TypeByExtension(path.Ext(href))
	}

	o.items = append(o.items, opfItem{id, href, mediaType, ""})
	return &o.items[len(o.items)-1]
}

// AddItemWithFallback records a file that has been placed in the zip archive for the ePub
// with the OPF manifset.  This method is very similar to AddItem, but also allows
// a fallback item to be specified.
func (o *OPF) AddItemWithFallback(id string, href string, mediaType string, fallback string) *opfItem {
	id = strings.TrimSpace(id)
	href = strings.TrimSpace(href)
	mediaType = strings.TrimSpace(mediaType)
	fallback = strings.TrimSpace(fallback)

	if mediaType == "" {
		mediaType = mime.TypeByExtension(path.Ext(href))
	}

	o.items = append(o.items, opfItem{id, href, mediaType, fallback})
	return &o.items[len(o.items)-1]
}

// AddItemRef inserts an entry into the spine for the ePub.
func (o *OPF) AddItemRef(idref string, linear bool) {
	idref = strings.TrimSpace(idref)
	o.itemrefs = append(o.itemrefs, opfItemRef{idref, linear})
}

// AddReference inserts an entry into the Guide for the ePub
func (o *OPF) AddReference(gt GuideType, title string, href string) {
	title = SimplifyCharset(strings.TrimSpace(title))
	href = strings.TrimSpace(href)

	o.references = append(o.references, opfReference{gt, title, href})
}

// GetManifestCount returns the number of items currently in the manifest.
func (o *OPF) GetManifestCount() int {
	return len(o.items)
}

// GetSpineCount returns the number of items currently in the spine.
func (o *OPF) GetSpineCount() int {
	return len(o.itemrefs)
}

// GetGuideCount returns the number of items currently in the guid.
func (o *OPF) GetGuideCount() int {
	return len(o.references)
}

func yesno(b bool) string {
	if b {
		return "yes"
	}
	return "no"
}

// Write create the OPF file, and insert that file into the zip archive for the
// ePub.  This should be the last file added to the archive.
func (o *OPF) Write(z *Writer) error {
	file := bytes.NewBuffer(nil)

	title := strings.TrimSpace(o.Title)
	language := strings.TrimSpace(o.Language)
	identifier := strings.TrimSpace(o.Identifier)
	if title == "" || language == "" || identifier == "" {
		return ErrIncompleteMetadata
	}

	file.WriteString("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
	file.WriteString("<package version=\"2.0\" unique-identifier=\"PrimaryID\" xmlns=\"http://www.idpf.org/2007/opf\">\n")
	file.WriteString("<metadata xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:opf=\"http://www.idpf.org/2007/opf\">\n")
	file.WriteString("<dc:title>" + title + "</dc:title>\n")
	file.WriteString("<dc:identifier id=\"PrimaryID\">" + identifier + "</dc:identifier>\n")
	file.WriteString("<dc:language>" + language + "</dc:language>\n")
	if o.Creator != "" {
		file.WriteString("<dc:creator opf:role=\"aut\" opf:file-as=\"" + o.Creator + "\">" + o.Creator + "</dc:creator>\n")
	}
	if o.Subject != "" {
		file.WriteString("<dc:subject>" + o.Subject + "</dc:subject>\n")
	}
	if o.Description != "" {
		file.WriteString("<dc:description>" + o.Description + "</dc:description>\n")
	}
	if o.Publisher != "" {
		file.WriteString("<dc:publisher>" + o.Publisher + "</dc:publisher>\n")
	}
	for _, v := range o.Contributor {
		file.WriteString("<dc:contributor>" + v + "</dc:contributor>\n")
	}
	if !o.Date.IsZero() {
		file.WriteString("<dc:date opf:event=\"original-publication\">" + o.Date.Format("2006") + "</dc:date>\n")
	}
	if o.Source != "" {
		file.WriteString("<dc:source>" + o.Source + "</dc:source>\n")
	}
	if o.Coverage != "" {
		file.WriteString("<dc:coverage>" + o.Coverage + "</dc:coverage>\n")
	}
	if o.Rights != "" {
		file.WriteString("<dc:rights>" + o.Rights + "</dc:rights>\n")
	}
	file.WriteString("</metadata>\n")

	file.WriteString("<manifest>\n")
	for _, item := range o.items {
		file.WriteString("<item id=\"" + item.id + "\" href=\"" + item.href + "\" media-type=\"" + item.mediaType + "\"")
		if item.fallback != "" {
			file.WriteString(" fallback=\"" + item.fallback + "\"")
		}
		file.WriteString("/>\n")
	}
	file.WriteString("</manifest>\n")

	file.WriteString("<spine toc=\"ncx\">\n")
	for _, item := range o.itemrefs {
		file.WriteString("<itemref idref=\"" + item.idRef + "\" linear=\"" + yesno(item.isLinear) + "\"/>\n")
	}
	file.WriteString("</spine>\n")

	file.WriteString("<guide>\n")
	for _, item := range o.references {
		file.WriteString("<reference type=\"" + item.Type.String() + "\" title=\"")
		EscapeString(file, item.Title)
		file.WriteString("\" href=\"" + item.Href + "\"/>\n")
	}
	file.WriteString("</guide>\n")

	_, err := file.WriteString("</package>\n")
	if err != nil {
		return err
	}

	// Now that the buffer has been initialized with the file contents
	err = z.DeflateEntry("index.opf", file.Bytes())
	if err != nil {
		return err
	}

	return nil
}
