package epub

import (
	"io"
	"strconv"
)

// SimplifyCharset replaces many characters in a string with a visually similar, but
// more common, character.  Some ePub readers may not support all of Unicode in
// parts of the UI.
func SimplifyCharset(text string) string {
	r := []rune(text)

	for i, c := range r {
		switch c {
		case 0x2018:
			r[i] = '\'' // left single quote
		case 0x2019:
			r[i] = '\'' // right single quote
		case 0x201C:
			r[i] = '"' // left double quote
		case 0x201D:
			r[i] = '"' // right double quote
		case 0x201B:
			r[i] = '\'' // single high reversed-9 quotation
		case 0x201F:
			r[i] = '"' // double high reversed 9 quotation
		case 0x2039:
			r[i] = '<' // single angle quote, left
		case 0x203A:
			r[i] = '>' // single angle quote, right

		case 0x2212:
			r[i] = '-' // minus sign
		case 0x2010:
			r[i] = '-' // hyphen
		case 0x2012:
			r[i] = '-' // dash
		case 0x2013:
			r[i] = '-' // en dash
		case 0x2014:
			r[i] = '-' // em dash
		case 0x2015:
			r[i] = '-' // quotation dash

		case '\t':
			r[i] = ' '
		case '\n':
			r[i] = ' '

		default: // do nothing
		}
	}
	text = string(r)
	return text
}

// Escape writes to w the properly escaped XML equivalent of the plain text data.
// This routine extends the behaviour of the function found in the package xml by
// also escaping the results so that output is in a 7-bit encoding.
func EscapeString(w io.Writer, s string) error {
	esc := []byte(nil)
	last := 0
	r := []rune(s)

	for i, c := range r {
		switch c {
		case '"':
			esc = []byte("&quot;")
		case '\'':
			esc = []byte("&apos;")
		case '&':
			esc = []byte("&amp;")
		case '<':
			esc = []byte("&lt;")
		case '>':
			esc = []byte("&gt;")
		default:
			if c > 0x7f {
				esc = []byte("&#" + strconv.Itoa(int(c)) + ";")
			} else {
				continue
			}
		}
		w.Write([]byte(string(r[last:i])))
		w.Write(esc)
		last = i + 1
	}
	_, err := w.Write([]byte(string(r[last:])))
	return err
}
