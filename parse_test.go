package epub

import (
	"testing"
)

func TestParseDateTime(t *testing.T) {
	cases := []struct {
		in   string
		year int
		out  int64
	}{
		{"2014", 2014, 1388534400},
		{"1997", 1997, 852076800},
		{"2015-04", 2015, 1427846400},
		{"1997-07", 1997, 867715200},
		{"2016-04-05", 2016, 1459814400},
		{"1997-07-16", 1997, 869011200},
		{"2016-04-05", 2016, 1459814400},
		{"1997-07-16T19:20+01:00", 1997, 869077200},
		{"1997-07-16T19:20:30+01:00", 1997, 869077230},
		{"1997-07-16T19:20:30.45+01:00", 1997, 869077230},
	}

	for _, v := range cases {
		p, err := ParseDateTime(v.in)
		if err != nil {
			t.Errorf("Unexpected parse failure, %s", err.Error)
		}
		if p.Year() != v.year {
			t.Errorf("got %d, expected %d", p.Year(), v.out)
		}
		if p.Unix() != v.out {
			t.Errorf("got %d, expected %d", p.Unix(), v.out)
		}
	}
}
