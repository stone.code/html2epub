package main

import (
	"bitbucket.org/rj/epub"
	"regexp"
	"strings"
)

var (
	HeaderNameRe = regexp.MustCompile("^[[:blank:]]*[[:alpha:]]([[:alnum:]]|[\\-])*[:]")
)

func Split1(s, sep string) (first, rest string) {
	tmp := strings.SplitN(s, sep, 2)
	if len(tmp) == 1 {
		return tmp[0], ""
	}
	return tmp[0], tmp[1]
}

func extractMetadata(s string) ([]string, string) {
	var ret []string

	line, rest := Split1(s, "\n")
	for len(line) > 0 && line[0] == '%' {
		ret = append(ret, line[1:])
		s = rest
		line, rest = Split1(rest, "\n")
	}
	return ret, s
}

func NewDC(doc string) (string, *epub.DublinCore, error) {
	meta, rest := extractMetadata(doc)

	ret := &epub.DublinCore{}
	for i, v := range meta {
		if HeaderNameRe.MatchString(v) {
			ndx := strings.Index(v, ":")
			key := strings.TrimSpace(v[:ndx])
			value := v[ndx+1:]
			err := ret.Set(key, value)
			if err != nil {
				return "", nil, err
			}
		} else if i < 3 {
			switch i {
			case 0:
				ret.Title = strings.TrimSpace(v)
			case 1:
				ret.Creator = strings.TrimSpace(v)
			case 2:
				tmp, err := epub.ParseDateTime(strings.TrimSpace(v))
				if err != nil {
					return "", nil, err
				}
				ret.Date = tmp
			}
		}
	}

	return rest, ret, nil
}
