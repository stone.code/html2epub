package main

import (
	"testing"
)

func TestSplit1(t *testing.T) {
	cases := []struct {
		in   string
		a, b string
	}{
		{"% A\n% B\n% C\n", "% A", "% B\n% C\n"},
	}

	for _, v := range cases {
		line, rest := Split1(v.in, "\n")
		if line != v.a {
			t.Errorf("Incorrect line, expected %s, got %s", v.a, line)
		}
		if rest != v.b {
			t.Errorf("Incorrect line, expected %s, got %s", v.b, rest)
		}
	}
}

func TestNewDC(t *testing.T) {
	rest, dc, err := NewDC(
		`% A Title
% Some Author
% 2109

# This is a story about...
`)

	if err != nil {
		t.Fatalf("Unexpected parsing error")
	}
	if dc.Title != "A Title" {
		t.Errorf("Incorrect title, got %s", dc.Title)
	}

	if rest != "\n# This is a story about...\n" {
		t.Errorf("Incorrect remainder text, %s", rest)
	}
}
