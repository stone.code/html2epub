package main

import (
	"archive/zip"
	"errors"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"
)

type Archive interface {
	Open(name string) (io.ReadCloser, error)
	Close() error
}

type folderArchive string

func NewFolderArchive(name string) (Archive, error) {
	absname, err := filepath.Abs(name)
	if err != nil {
		return nil, err
	}
	dir, _ := filepath.Split(absname)
	return folderArchive(dir), nil
}

func (p folderArchive) Open(name string) (io.ReadCloser, error) {
	file, err := os.Open(string(p) + string(filepath.Separator) + name)
	return file, err
}

func (folderArchive) Close() error {
	return nil
}

type nopArchive struct{}

func NewNopArchive() Archive {
	return &nopArchive{}
}

func (*nopArchive) Open(name string) (io.ReadCloser, error) {
	return nil, errors.New("Tried to open a file from a no-op archive.")
}

func (*nopArchive) Close() error {
	return nil
}

type httpArchive struct {
	http.Client
	baseurl string
}

func NewHttpArchive(url string) (Archive, error) {
	absname := path.Clean(url)
	if absname[len(absname)-1] != '/' {
		dir, _ := path.Split(absname)
		absname = dir + "/"
	}
	return &httpArchive{baseurl: absname}, nil
}

func (h *httpArchive) Open(name string) (io.ReadCloser, error) {
	r, err := h.Get(h.baseurl + name)
	if err != nil {
		return nil, err
	}
	return r.Body, nil
}

func (*httpArchive) Close() error {
	return nil
}

type zipArchive struct {
	z     *zip.ReadCloser
	files map[string]*zip.File
}

func NewZipArchive(name string) (Archive, error) {
	z, err := zip.OpenReader(name)
	if err != nil {
		return nil, err
	}

	files := make(map[string]*zip.File)
	for i := 0; i < len(z.File); i++ {
		name := z.File[i].Name
		files[name] = z.File[i]
	}

	return &zipArchive{z, files}, nil
}

func (z *zipArchive) Open(name string) (io.ReadCloser, error) {
	if f, ok := z.files[name]; ok {
		r, err := f.Open()
		return r, err
	}

	return nil, os.ErrNotExist
}

func (z *zipArchive) Close() error {
	return z.z.Close()
}
