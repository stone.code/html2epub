package main

import (
	"bytes"
	"encoding/base64"
	"io"
	"net/http"
	"regexp"
	"strings"
)

func isDataUri(src string) bool {
	re, _ := regexp.Compile("data:[a-z]+/[a-z]+;base64,")
	loc := re.FindStringIndex(src)
	return loc != nil
}

func isHttpUri(src string) bool {
	return src[0:7] == "http://" || src[0:8] == "https://"
}

func copyDataUri(dst io.Writer, src string) error {
	// Strip of URI type specifier, mimetype, and encoding
	ndx := strings.Index(src, ",")
	if ndx < 1 {
		panic("Internal error.")
	}
	src = src[ndx+1:]

	// Do we need to strip out internal whitespace?
	// Unfortunately, the decode provided by Go does not
	// ignore embedded white space
	if strings.IndexAny(src, " \n\t\r") >= 0 {
		buffer := make([]byte, 1024)
		buffer = buffer[:0]

		for i := 0; i < len(src); i++ {
			switch src[i] {
			case ' ', '\n', '\t', '\r':
				// do nothing
			default:
				buffer = append(buffer, src[i])
			}
		}

		src = string(buffer)
	}

	reader := strings.NewReader(src)
	decoder := base64.NewDecoder(base64.StdEncoding, reader)

	_, err := io.Copy(dst, decoder)
	return err
}

func copyFile(dst io.Writer, src string, ar Archive) error {
	infile, err := ar.Open(src)
	if err != nil {
		return err
	}
	defer infile.Close()

	_, err = io.Copy(dst, infile)
	return err
}

func copyHttpUri(dst io.Writer, src string) error {
	r, err := http.Get(src)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	_, err = io.Copy(dst, r.Body)
	return err
}

type ArchiveLoader struct {
	Archive Archive
}

func (ar *ArchiveLoader) Load(src string) ([]byte, error) {
	dst := bytes.NewBuffer(nil)
	if isDataUri(src) {
		err := copyDataUri(dst, src)
		return dst.Bytes(), err
	}
	if isHttpUri(src) {
		err := copyHttpUri(dst, src)
		return dst.Bytes(), err
	}

	// assume that it is a normal file link
	err := copyFile(dst, src, ar.Archive)
	return dst.Bytes(), err
}

func Copy(dst io.Writer, src string, ar Archive) error {
	if isDataUri(src) {
		return copyDataUri(dst, src)
	}
	if isHttpUri(src) {
		return copyHttpUri(dst, src)
	}

	// assume that it is a normal file link
	return copyFile(dst, src, ar)
}
