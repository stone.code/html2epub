package main

import (
	"bitbucket.org/rj/epub"
	"bytes"
	"flag"
	"fmt"
	"github.com/russross/blackfriday"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

const (
	html_open = `<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
`
	html_close = `</html>`
)

var (
	infilename   = flag.String("in", "", "Input filename.")
	outfilename  = flag.String("out", "", "Output filename.")
	htmlfilename = flag.String("html", "", "HTML (intermediate) filename.")
	headinglevel = flag.Int("headinglevel", 1, "Determine depth of heading level placed in table of contents.")
	verbose      = flag.Bool("v", false, "Set verbose output.")
	findHeading  = regexp.MustCompile("[[:blank:]]*[\\n][\\r]?((#+[[:blank:]]*[^\\n\\r]+)|([[:blank:]]*[^\\n\\r]+[\\n][\\r]?[=]+)|([[:blank:]]*[^\\n\\r]+[\\n][\\r]?[-]+))")
)

func main() {
	// Parse and check command-line options
	flag.Parse()
	if *infilename == "" {
		fmt.Println("Input filename expected, but not provided.")
		os.Exit(1)
		return
	}
	if *outfilename == "" {
		*outfilename = GetFilenameForEpub(*infilename)
		if *verbose {
			fmt.Println("Setting output filename to: ", outfilename)
		}
	}
	if *headinglevel < 1 || *headinglevel > 7 {
		fmt.Println("Heading level set to an incorrect value.  Acceptable values are between 1 and 7.")
		os.Exit(1)
		return
	}

	// Open the input file for reading
	// Also collect information so that additional resources can be loaded
	// if necessary (href).
	if *verbose {
		fmt.Printf("Reading %s.\n", *infilename)
	}
	file, ar, err := open_input(*infilename)
	if err != nil {
		panic(err)
	}
	defer ar.Close()
	defer file.Close()
	doc, err := read_all(file)
	if err != nil {
		panic(err)
	}

	if *verbose {
		fmt.Printf("Exacting meta data.\n")
	}
	doc, dc_info, err := NewDC(doc)
	if err != nil {
		panic(err)
	}
	dc_info.Normalize()
	if dc_info.Language == "" {
		dc_info.Language = "en"
	}
	if dc_info.Identifier == "" {
		dc_info.Identifier = "NA"
	}
	if *verbose {
		fmt.Printf("\t%s\n\t%s\n\t%s\n", dc_info.Title, dc_info.Creator, dc_info.Date)
	}

	if *verbose {
		fmt.Printf("Processing HTML.\n")
	}
	if *htmlfilename != "" {
		save_intermediate_html(*htmlfilename, doc)
	}

	// Open the output file
	*outfilename, err = filepath.Abs(*outfilename)
	if err != nil {
		panic(err)
	}
	file, writer, err := open_output(*outfilename)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	defer writer.Close()

	// Create the components
	epub.CreateMimeTypeFile(writer)
	epub.CreateMetaInf(writer)
	opf := epub.NewOPFWithDublinCore(dc_info)
	// CopyImages(&opf, writer, doc, ar)
	epub.CreateMakefile(writer, &opf, *outfilename)
	ncx := epub.NewNCX()
	CreateCoverOrTitlePage(writer, &opf, &ncx, ar)
	CreateChapters(writer, &opf, &ncx, doc)
	err = ncx.Write(writer, &opf)
	if err != nil {
		panic(err)
	}
	err = opf.Write(writer) // all files should be created before call to CreateOpf
	if err != nil {
		panic(err)
	}
}

func open_input(infilename string) (io.ReadCloser, Archive, error) {
	// Check for an online resource
	if infilename[0:7] == "http://" || infilename[0:8] == "https://" {
		// Access the resource
		r, err := http.Get(infilename)
		if err != nil {
			return nil, nil, err
		}

		// Create an archive interface
		ar, err := NewHttpArchive(infilename)
		if err != nil {
			r.Body.Close()
			return nil, nil, err
		}

		return r.Body, ar, nil
	}

	// Check for a zip file
	if filepath.Ext(infilename) == ".zip" {
		// Access the resource
		ar, err := NewZipArchive(infilename)
		if err != nil {
			return nil, nil, err
		}

		// Locate the main HTML file
		h, err := ar.Open("index.html")
		if err == nil {
			return h, ar, nil
		}
		if err != os.ErrNotExist {
			return nil, nil, err
		}

		h, err = ar.Open("index.htm")
		if err == nil {
			return h, ar, nil
		}
		if err != os.ErrNotExist {
			return nil, nil, err
		}

		h, err = ar.Open(filepath.Base(infilename)[:len(infilename)-4] + ".html")
		if err == nil {
			return h, ar, nil
		}
		if err != os.ErrNotExist {
			return nil, nil, err
		}

		h, err = ar.Open(filepath.Base(infilename)[:len(infilename)-4] + ".htm")
		if err == nil {
			return h, ar, nil
		}
		if err != os.ErrNotExist {
			return nil, nil, err
		}

		return nil, nil, os.ErrNotExist
	}

	// Open the request file
	file, err := os.Open(infilename)
	if err != nil {
		return nil, nil, err
	}

	// Create an archive interface
	// This will allow relative references from with the HTML
	// to be located
	ar, err := NewFolderArchive(infilename)
	if err != nil {
		file.Close()
		return nil, nil, err
	}

	return file, ar, nil
}

func read_all(in io.Reader) (string, error) {
	doc := bytes.NewBuffer(nil)
	_, err := io.Copy(doc, in)
	if err != nil {
		return "", err
	}

	return string(doc.Bytes()), nil
}

func save_intermediate_html(name string, doc string) error {
	// First, create the file
	file, err := os.OpenFile(name, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}

	// Write the HTML
	_, err = file.WriteString(doc)
	return err
}

func open_output(name string) (*os.File, *epub.Writer, error) {
	// First, create the file
	file, err := os.OpenFile(name, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		return nil, nil, err
	}

	// Second, initialize the ZIP writer
	w := epub.NewWriter(file)
	return file, w, nil
}

func GetFilenameForEpub(filename string) string {
	switch filepath.Ext(filename) {

	case ".md":
		return filename[0:len(filename)-2] + "epub"

	case ".txt":
		return filename[0:len(filename)-3] + "epub"

	case ".text":
		return filename[0:len(filename)-4] + "epub"

	case ".zip":
		return filename[0:len(filename)-3] + "epub"

	}

	return filename + ".epub"
}

func CreateCoverOrTitlePage(z *epub.Writer, opf *epub.OPF, ncx *epub.NCX, ar Archive) {
	if opf.CoverURL == "" {
		epub.CreateTitlePage(z, opf, ncx)
	} else {
		epub.CreateCover(z, opf, ncx, &ArchiveLoader{ar})
	}
}

func adjustStart(doc string) string {
	// See definition of regular expression used for find headings
	ndx := strings.IndexByte(doc, '\n')
	doc = doc[ndx+1:]
	if doc[0] == '\r' {
		doc = doc[1:]
	}
	return doc
}

func extractChapterHeading(doc string) (string, int) {
	if doc[0] == '#' {
		// This is a prefixed heading element
		i := 0
		// No need to check for hitting the end of the string.  The regexp matching
		// ensures that there will be text following the hashes.
		for doc[i] == '#' {
			i++
		}
		level := i
		title := doc[i:]
		// Trim to the end of the line
		ndx := strings.IndexAny(title, "\n\r")
		title = title[:ndx]
		// Done
		return strings.TrimSpace(title), level
	}

	// This is a postfix heading element, either level 1 or level 2
	ndx := strings.IndexAny(doc, "\n\r")
	pos := ndx + 1
	for doc[pos] == '\n' || doc[pos] == '\r' {
		pos++
	}
	if doc[pos] == '=' {
		return doc[:ndx], 1
	}
	if doc[pos] == '-' {
		return doc[:ndx], 2
	}
	panic("Internal error")
}

func CreateChapter(z *epub.Writer, opf *epub.OPF, ncx *epub.NCX, doc string, chapter int) error {
	doc = adjustStart(doc)

	chapter_id := "chapter" + strconv.FormatInt(int64(chapter), 10)
	chapter_file := "chapter" + strconv.FormatInt(int64(chapter), 10) + ".xhtml"
	chapter_title, chapter_lvl := extractChapterHeading(doc)

	// Clean up the title
	chapter_title = strings.TrimSpace(chapter_title)
	// Some titles shout (are all caps).  If so, fix
	if chapter_title == strings.ToUpper(chapter_title) {
		// Don't know why, but the string must be converted to lowercase first.
		// Otherwise, conversion by Title does nothing
		chapter_title = strings.Title(strings.ToLower(chapter_title))
	}

	file := bytes.NewBuffer(nil)
	file.Write([]byte(html_open))
	file.Write([]byte("<head><title>"))
	epub.EscapeString(file, chapter_title)
	file.Write([]byte("</title></head>\n"))
	file.Write([]byte("<body>\n"))
	html := blackfriday.MarkdownCommon([]byte(doc))
	_, err := file.Write(html)
	if err != nil {
		return err
	}
	file.Write([]byte("</body>\n"))
	file.Write([]byte(html_close))

	err = z.DeflateEntry(chapter_file, file.Bytes())
	if err != nil {
		return err
	}

	opf.AddItem(chapter_id, chapter_file, "application/xhtml+xml")
	opf.AddItemRef(chapter_id, true)
	opf.AddReference(epub.GuideText, chapter_title, chapter_file)
	ncx.AddNavPoint(chapter_title, chapter_file, chapter_id)

	return nil
}

func CreateChapters(z *epub.Writer, opf *epub.OPF, ncx *epub.NCX, doc string) error {
	boundaries := findHeading.FindAllStringIndex(doc, -1)
	if len(boundaries) == 0 {
		html := blackfriday.MarkdownCommon([]byte(doc))

		file := bytes.NewBuffer(nil)
		file.Write([]byte(html_open))
		file.Write([]byte("<head><title>" + opf.Title + "</title></head>\n"))
		file.Write([]byte("<body>\n"))
		file.Write(html)
		file.Write([]byte("</body>\n"))
		_, err := file.Write([]byte(html_close))
		if err != nil {
			return err
		}
		err = z.DeflateEntry("all.xhtml", file.Bytes())
		if err != nil {
			return err
		}

		opf.AddItem("all", "all.xhtml", "application/xhtml+xml")
		opf.AddItemRef("all", true)
		opf.AddReference(epub.GuideForeword, "All", "all.xhtml")
		ncx.AddNavPoint("All", "all.xhtml", "all")
		return nil
	}

	end := boundaries[0][0]
	text := doc[:end]
	html := blackfriday.MarkdownCommon([]byte(text))
	if len(html) > 0 {
		file := bytes.NewBuffer(nil)
		file.Write([]byte(html_open))
		file.Write([]byte("<head><title>" + opf.Title + "</title></head>\n"))
		file.Write([]byte("<body>\n"))
		file.Write(html)
		file.Write([]byte("</body>\n"))
		_, err := file.Write([]byte(html_close))
		if err != nil {
			return err
		}
		err = z.DeflateEntry("forward.xhtml", file.Bytes())
		if err != nil {
			return err
		}

		opf.AddItem("forward", "forward.xhtml", "application/xhtml+xml")
		opf.AddItemRef("forward", true)
		opf.AddReference(epub.GuideForeword, "Forward", "forward.xhtml")
		ncx.AddNavPoint("Forward", "forward.xhtml", "forward")
	}

	for i := 0; i < len(boundaries)-1; i++ {
		start := boundaries[i][0]
		end := boundaries[i+1][0]

		err := CreateChapter(z, opf, ncx, doc[start:end], i+1)
		if err != nil {
			return err
		}
	}

	start := boundaries[len(boundaries)-1][0]
	err := CreateChapter(z, opf, ncx, doc[start:], len(boundaries))
	if err != nil {
		return err
	}

	return nil
}
