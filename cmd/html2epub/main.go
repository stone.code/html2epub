package main

import (
	"bitbucket.org/rj/epub"
	dom "bitbucket.org/rj/xmldom-go"
	"bytes"
	"flag"
	"fmt"
	"io"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var (
	infilename   = flag.String("in", "", "Input filename.")
	outfilename  = flag.String("out", "", "Output filename.")
	htmlfilename = flag.String("html", "", "HTML (intermediate) filename.")
	filters      = flag.String("filters", "", "Filters to apply on document.")
	headinglevel = flag.Int("headinglevel", 1, "Determine depth of heading level placed in table of contents.")
	verbose      = flag.Bool("v", false, "Set verbose output.")
)

const (
	html_open = `<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
`
	html_close = `</html>`
)

func GetFilenameForEpub(filename string) string {
	switch filepath.Ext(filename) {

	case ".htm":
		return filename[0:len(filename)-3] + "epub"

	case ".html":
		return filename[0:len(filename)-4] + "epub"

	case ".zip":
		return filename[0:len(filename)-3] + "epub"

	}

	return filename + ".epub"
}

func open_input(infilename string) (io.ReadCloser, Archive, error) {
	// Check for an online resource
	if infilename[0:7] == "http://" || infilename[0:8] == "https://" {
		// Access the resource
		r, err := http.Get(infilename)
		if err != nil {
			return nil, nil, err
		}

		// Create an archive interface
		ar, err := NewHttpArchive(infilename)
		if err != nil {
			r.Body.Close()
			return nil, nil, err
		}

		return r.Body, ar, nil
	}

	// Check for a zip file
	if filepath.Ext(infilename) == ".zip" {
		// Access the resource
		ar, err := NewZipArchive(infilename)
		if err != nil {
			return nil, nil, err
		}

		// Locate the main HTML file
		h, err := ar.Open("index.html")
		if err == nil {
			return h, ar, nil
		}
		if err != os.ErrNotExist {
			return nil, nil, err
		}

		h, err = ar.Open("index.htm")
		if err == nil {
			return h, ar, nil
		}
		if err != os.ErrNotExist {
			return nil, nil, err
		}

		h, err = ar.Open(filepath.Base(infilename)[:len(infilename)-4] + ".html")
		if err == nil {
			return h, ar, nil
		}
		if err != os.ErrNotExist {
			return nil, nil, err
		}

		h, err = ar.Open(filepath.Base(infilename)[:len(infilename)-4] + ".htm")
		if err == nil {
			return h, ar, nil
		}
		if err != os.ErrNotExist {
			return nil, nil, err
		}

		return nil, nil, os.ErrNotExist
	}

	// Open the request file
	file, err := os.Open(infilename)
	if err != nil {
		return nil, nil, err
	}

	// Create an archive interface
	// This will allow relative references from with the HTML
	// to be located
	ar, err := NewFolderArchive(infilename)
	if err != nil {
		file.Close()
		return nil, nil, err
	}

	return file, ar, nil
}

func save_intermediate_html(name string, doc *dom.Document) error {
	// First, create the file
	file, err := os.OpenFile(name, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}

	// Write the HTML
	_, err = file.Write(doc.DocumentElement().ToXml())
	return err
}

func open_output(name string) (*os.File, *epub.Writer, error) {
	// First, create the file
	file, err := os.OpenFile(name, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		return nil, nil, err
	}

	// Second, initialize the ZIP writer
	w := epub.NewWriter(file)
	return file, w, nil
}

func CopyImages(opf *epub.OPF, z *epub.Writer, doc *dom.Document, ar Archive) {
	// Remember which images we've already copied
	// Some resources may be used multiple times
	images := map[string]int{}
	comment := "Image files"

	// Copy all images
	e := doc.DocumentElement()
	items := e.GetElementsByTagName("img")
	for lp := items.Length(); lp > 0; lp-- {
		i := items.Item(lp - 1).(*dom.Element)
		src := i.GetAttribute("src")
		if ndx, ok := images[src]; !ok {
			ndx = len(images) + 1
			dst := bytes.NewBuffer(nil)
			err := Copy(dst, src, ar)
			if err != nil {
				panic(err)
			}
			_, err = z.StoreEntry("images/i"+strconv.Itoa(ndx)+filepath.Ext(src), dst.Bytes())
			if err != nil {
				panic(err)
			}
			images[src] = ndx
			i.SetAttribute("src", "images/i"+strconv.Itoa(ndx)+filepath.Ext(src))
			// Add the image to the manifest
			opf.AddItem("image"+strconv.Itoa(ndx), "images/i"+strconv.Itoa(ndx)+filepath.Ext(src), mime.TypeByExtension(filepath.Ext(src)), comment)
			comment = ""
		} else {
			i.SetAttribute("src", "images/i"+strconv.Itoa(ndx)+filepath.Ext(src))
		}
	}
}

func CreateCoverOrTitlePage(opf *epub.OPF, ncx *epub.NCX, z *epub.Writer, dc *epub.DublinCore, ar Archive) {
	if dc.CoverURL == "" {
		epub.CreateTitlePage(opf, ncx, z)
	} else {
		epub.CreateCover(opf, ncx, z, dc, &ArchiveLoader{ar})
	}
}

func is_heading(n dom.Node) bool {
	// If node is not an element, it can't be a heading
	if n.NodeType() != dom.ELEMENT_NODE {
		return false
	}

	// Check that element name matches possible heading
	if n.NodeName()[0] != 'h' || len(n.NodeName()) != 2 {
		return false
	}

	// Get the heading level
	level := int(n.NodeName()[1]) - int('0')
	return level >= 1 && level <= *headinglevel
}

func CreateChapter(opf *epub.OPF, ncx *epub.NCX, children dom.NodeList, z *epub.Writer, chapter uint, lp uint) uint {
	c := children.Item(lp)

	chapter_id := "chapter" + strconv.FormatUint(uint64(chapter), 10)
	chapter_file := "chapter" + strconv.FormatUint(uint64(chapter), 10) + ".xhtml"
	chapter_title := string(c.(*dom.Element).ToText(false))

	// Clean up the title
	chapter_title = strings.TrimSpace(chapter_title)
	// Some titles shout (are all caps).  If so, fix
	if chapter_title == strings.ToUpper(chapter_title) {
		// Don't know why, but the string must be converted to lowercase first.
		// Otherwise, conversion by Title does nothing
		chapter_title = strings.Title(strings.ToLower(chapter_title))
	}

	file := bytes.NewBuffer(nil)
	file.Write([]byte(html_open))
	file.Write([]byte("<head><title>"))
	epub.EscapeString(file, chapter_title)
	file.Write([]byte("</title></head>\n"))
	file.Write([]byte("<body>\n"))
	file.Write(c.(*dom.Element).ToXml())
	_, err := file.Write([]byte("\n"))
	if err != nil {
		panic(err)
	}

	lp++
	for lp < children.Length() && !is_heading(children.Item(lp)) {
		c = children.Item(lp)
		switch c.NodeType() {
		case dom.ELEMENT_NODE:
			file.Write(c.(*dom.Element).ToXml())
		case dom.TEXT_NODE:
			txt := strings.TrimSpace(c.NodeValue())
			if txt != "" {
				file.Write([]byte("<p>" + txt + "</p>"))
			}
		}
		lp++
	}
	file.Write([]byte("</body>\n"))
	file.Write([]byte(html_close))
	_, err = z.DeflateEntry(chapter_file, file.Bytes())
	if err != nil {
		panic(err)
	}

	opf.AddItem(chapter_id, chapter_file, "application/xhtml+xml", "")
	opf.AddItemRef(chapter_id, true)
	opf.AddReference(epub.GuideText, chapter_title, chapter_file)
	ncx.AddNavPoint(chapter_title, chapter_file, chapter_id)

	return lp
}

func CreateChapters(opf *epub.OPF, ncx *epub.NCX, z *epub.Writer, doc *dom.Document) {
	// Grab the body element
	if doc.DocumentElement().GetElementsByTagName("body").Length() != 1 {
		panic("Improperly formatted HTML document.  Not appropriate count of body elements.")
	}

	body := doc.DocumentElement().GetElementsByTagName("body").Item(0).(*dom.Element)
	children := body.ChildNodes()

	lp := uint(0)
	if !is_heading(children.Item(0)) {
		file := bytes.NewBuffer(nil)

		file.Write([]byte(html_open))
		file.Write([]byte("<head><title>" + opf.Title + "</title></head>\n"))
		file.Write([]byte("<body>\n"))
		for lp < children.Length() && !is_heading(children.Item(lp)) {
			c := children.Item(lp)
			switch c.NodeType() {
			case dom.ELEMENT_NODE:
				file.Write(c.(*dom.Element).ToXml())
			case dom.TEXT_NODE:
				txt := strings.TrimSpace(c.NodeValue())
				if txt != "" {
					file.Write([]byte("<p>" + txt + "</p>"))
				}
			}
			lp++
		}
		file.Write([]byte("</body>\n"))
		_, err := file.Write([]byte(html_close))
		if err != nil {
			panic(err)
		}
		_, err = z.DeflateEntry("forward.xhtml", file.Bytes())
		if err != nil {
			panic(err)
		}

		opf.AddItem("forward", "forward.xhtml", "application/xhtml+xml", "Forward")
		opf.AddItemRef("forward", true)
		opf.AddReference(epub.GuideForeword, "Forward", "forward.xhtml")
		ncx.AddNavPoint("Forward", "forward.xhtml", "forward")
	}

	chapter := uint(1)
	for lp < children.Length() {
		lp = CreateChapter(opf, ncx, children, z, chapter, lp)
		chapter++
	}
}

func main() {
	// Parse and check command-line options
	flag.Parse()
	if *infilename == "" {
		fmt.Println("Input filename expected, but not provided.")
		os.Exit(1)
		return
	}
	if *outfilename == "" {
		*outfilename = GetFilenameForEpub(*infilename)
		if *verbose {
			fmt.Println("Setting output filename to: ", outfilename)
		}
	}
	if *headinglevel < 1 || *headinglevel > 7 {
		fmt.Println("Heading level set to an incorrect value.  Acceptable values are between 1 and 7.")
		os.Exit(1)
		return
	}

	// Open the input file for reading
	// Also collect information so that additional resources can be loaded
	// if necessary (href).
	if *verbose {
		fmt.Printf("Reading %s.\n", *infilename)
	}
	file, ar, err := open_input(*infilename)
	if err != nil {
		panic(err)
	}
	defer ar.Close()
	defer file.Close()
	doc, err := dom.ParseHtml(file)
	if err != nil {
		panic(err)
	}

	if *verbose {
		fmt.Printf("Exacting meta data.\n")
	}
	dc_info, err := NewDC(doc)
	if err != nil {
		panic(err)
	}
	dc_info.DefaultLanguage("en").DefaultIdentifier("NA")
	if *verbose {
		fmt.Printf("\t%s\n\t%s\n\t%s\n", dc_info.Title, dc_info.Author, dc_info.Date)
	}

	if *verbose {
		fmt.Printf("Processing HTML.\n")
	}
	StripScriptElementsFromHead(doc)
	StripStylesheetElementsFromHead(doc)
	StripLeadingWhitespace(doc)
	ApplyFilters(doc, *filters)
	if *htmlfilename != "" {
		save_intermediate_html(*htmlfilename, doc)
	}

	// Open the output file
	*outfilename, err = filepath.Abs(*outfilename)
	if err != nil {
		panic(err)
	}
	file, writer, err := open_output(*outfilename)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	defer writer.Close()

	// Create the components
	epub.CreateMimeTypeFile(writer)
	epub.CreateMetaInf(writer)
	opf := epub.NewOPFWithDublinCore(dc_info)
	CopyImages(&opf, writer, doc, ar)
	epub.CreateMakefile(&opf, writer, *outfilename)
	ncx := epub.NewNCX()
	CreateCoverOrTitlePage(&opf, &ncx, writer, dc_info, ar)
	CreateChapters(&opf, &ncx, writer, doc)
	err = ncx.Write(writer, &opf)
	if err!=nil {
		panic(err)
	}
	err = opf.Write(writer) // all files should be created before call to CreateOpf
	if err!=nil {
		panic(err)
	}
}
