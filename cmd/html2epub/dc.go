package main

import (
	"bitbucket.org/rj/epub"
	"bitbucket.org/rj/xmldom-go"
	"errors"
	"path"
	"strings"
	"time"
)

type BadParseOfTitle struct{}

func (_ BadParseOfTitle) Error() string {
	return "Could not extract 'title' element from HTML head"
}

type BadParseOfCoverURL struct{}

func (_ BadParseOfCoverURL) Error() string {
	return "Could not extract 'cover' element from HTML head (missing mimetype?)"
}

func lookupTitle(head *dom.Element) (string, error) {
	// Find title
	nl := head.GetElementsByTagName("title")
	if nl.Length() != 1 {
		return "", BadParseOfTitle{}
	}
	elem := nl.Item(0)
	if elem.ChildNodes().Length() != 1 {
		return "", BadParseOfTitle{}
	}
	if elem.ChildNodes().Item(0).NodeType() != dom.TEXT_NODE {
		return "", BadParseOfTitle{}
	}
	title := elem.ChildNodes().Item(0).NodeValue()
	return title, nil
}

func lookupAuthor(head *dom.Element) (string, error) {
	// Find requested element
	nl := head.GetElementsByTagName("meta")
	for lp := uint(0); lp < nl.Length(); lp++ {
		elem := nl.Item(lp).(*dom.Element)
		if elem.GetAttribute("name") == "author" {
			return elem.GetAttribute("content"), nil
		}
	}

	// Author not found
	return "", nil
}

func lookupCover(head *dom.Element) (string, string, error) {
	// Find requested element
	nl := head.GetElementsByTagName("link")
	for lp := uint(0); lp < nl.Length(); lp++ {
		elem := nl.Item(lp).(*dom.Element)
		if elem.GetAttribute("rel") == "cover" {
			typ := elem.GetAttribute("type")
			ref := elem.GetAttribute("href")

			switch typ {
			case "":
				switch path.Ext(ref) {
				case ".png":
					return ref, "image/png", nil
				case ".jpg", ".jpeg":
					return ref, "image/jpeg", nil
				default:
					return "", "", errors.New("Mimetype for the cover image not recognized.  Image should be PNG or JPEG.")
				}
			case "image/png", "image/jpeg":
				return ref, typ, nil
			case "image/jpg":
				return ref, "image/jpeg", nil
			default:
				return "", "", errors.New("Mimetype for the cover image not recognized.  Image should be PNG or JPEG.")
			}
		}
	}

	// Cover not found
	return "", "", nil
}

func NewDC(doc *dom.Document) (*epub.DublinCore, error) {
	head, err := GetHead(doc)
	if err != nil {
		return nil, err
	}
	meta := head.GetElementsByTagName("meta")

	title, err := lookupTitle(head)
	if err != nil {
		return nil, err
	}
	author, err := lookupAuthor(head)
	if err != nil {
		return nil, err
	}
	cover, covertype, err := lookupCover(head)
	if err != nil {
		return nil, err
	}

	ret := &epub.DublinCore{
		Title:         strings.TrimSpace(title),
		Author:        strings.TrimSpace(author),
		CoverURL:         strings.TrimSpace(cover),
		CoverMimeType: strings.TrimSpace(covertype),
	}

	// Lookup title
	for lp := uint(0); lp < meta.Length(); lp++ {
		elem := meta.Item(lp).(*dom.Element)
		name := strings.ToLower(elem.GetAttribute("name"))

		switch name {
		case "dc.creator":
			ret.Creator = append(ret.Creator, strings.TrimSpace(elem.GetAttribute("content")))
		case "dc.date":
			if date, err := epub.ParseDateTime(elem.GetAttribute("content")); err == nil {
				ret.Date = date
			} else {
				return nil, err
			}
		case "dc.identifier":
			ret.Identifier = strings.TrimSpace(elem.GetAttribute("content"))
		}
	}

	if len(ret.Creator) == 0 && ret.Author != "" {
		ret.Creator = []string{ret.Author}
	}
	if ret.Author == "" && len(ret.Creator) > 0 {
		if len(ret.Creator) == 1 {
			ret.Author = ret.Creator[0]
		} else {
			ndx := len(ret.Creator) - 1
			ret.Author = strings.Join(ret.Creator[0:ndx], ", ") + " & " + ret.Creator[ndx]
		}
	}
	if ret.Identifier == "" {
		ret.Identifier = "unknown"
	}
	if ret.Date.IsZero() {
		ret.Date = time.Now()
	}
	if ret.CoverURL != "" && ret.CoverMimeType == "" {
		return nil, BadParseOfCoverURL{}
	}

	return ret, nil
}
