package main

import (
	"bitbucket.org/rj/xmldom-go"
	"fmt"
	"strings"
	"unicode"
)

func canHoist(cn dom.NodeList) (*dom.Element, bool) {
	i := uint(0)

	// Skip over leading white space text element
	if cn.Length() > i && cn.Item(i).NodeType() == dom.TEXT_NODE &&
		strings.TrimSpace(cn.Item(i).NodeValue()) == "" {
		i++
	}

	// Are we looking at a div?
	if cn.Length() > i && cn.Item(i).NodeType() == dom.ELEMENT_NODE && cn.Item(i).NodeName() == "div" {
		i++
	} else {
		return nil, false
	}
	elem := cn.Item(i - 1).(*dom.Element)

	// Skip over a trailing white space text elemgnt
	if cn.Length() > i && cn.Item(i).NodeType() == dom.TEXT_NODE &&
		strings.TrimSpace(cn.Item(i).NodeValue()) == "" {
		i++
	}

	if i == cn.Length() {
		return elem, true
	}

	return nil, false
}

func applyHoist(doc *dom.Document) {
	body, err := GetBody(doc)
	if err != nil {
		panic(err)
	}

	cn := body.ChildNodes()
	if e, ok := canHoist(cn); ok {
		e_count := body.ChildNodes().Length()

		// Move child nodes to the end of the body element
		for e.HasChildNodes() {
			n := e.ChildNodes().Item(0)
			e.RemoveChild(n)
			body.AppendChild(n)
		}

		// Remove original nodes
		for e_count > 0 {
			body.RemoveChild(body.ChildNodes().Item(0))
			e_count--
		}
	}
}

func applyMDashOnText(t *dom.Text) {
	// get unicode code points
	u := []rune(t.String())

	dash := false

	for lp := 0; lp < len(u); lp++ {
		switch u[lp] {

		case '-':
			if dash {
				// Convert previous dash to emdash
				u[lp-1] = 0x2014
				// eliminate this character
				u = append(u[0:lp], u[lp+1:]...)
				lp--
			} else {
				dash = true
			}
		default:
			dash = false
		}
	}

	t.SetData(string(u))
}

func applyMDashOnElem(e *dom.Element) {
	children := e.ChildNodes()

	for lp := uint(0); lp < children.Length(); lp++ {
		item := children.Item(lp)
		switch item.NodeType() {

		case dom.TEXT_NODE:
			applyMDashOnText(item.(*dom.Text))

		case dom.ELEMENT_NODE:
			applyMDashOnElem(item.(*dom.Element))

		}
	}
}

func applyMDash(doc *dom.Document) {
	applyMDashOnElem(doc.DocumentElement())
}

func applyReplaceHr(doc *dom.Document) {
	nodes := doc.DocumentElement().GetElementsByTagName("hr")
	for lp := uint(0); lp < nodes.Length(); lp++ {
		item := nodes.Item(lp)
		parent := item.ParentNode()

		tn := doc.CreateTextNode("* * *")
		pn := doc.CreateElement("p")
		pn.AppendChild(tn)
		pn.SetAttribute("style", "text-align:center")
		parent.ReplaceChild(pn, item)
	}
}

func select_rune(space bool, runea, runeb rune) rune {
	if space {
		return runea
	}
	return runeb
}

func applySmartQuotesOnText(t *dom.Text) {
	// get unicode code points
	u := []rune(t.String())

	space := true

	for lp := 0; lp < len(u); lp++ {
		switch u[lp] {

		case '\'':
			u[lp] = select_rune(space, '\u2018', '\u2019')
			space = false
		case '"':
			u[lp] = select_rune(space, '\u201C', '\u201D')
			space = false
		default:
			space = unicode.IsSpace(u[lp])
		}
	}

	t.SetData(string(u))
}

func applySmartQuotesOnElem(e *dom.Element) {
	children := e.ChildNodes()

	for lp := uint(0); lp < children.Length(); lp++ {
		item := children.Item(lp)
		switch item.NodeType() {

		case dom.TEXT_NODE:
			applySmartQuotesOnText(item.(*dom.Text))

		case dom.ELEMENT_NODE:
			applySmartQuotesOnElem(item.(*dom.Element))

		}
	}
}

func applySmartQuotes(doc *dom.Document) {
	applySmartQuotesOnElem(doc.DocumentElement())
}

func applyStripEmptyPara(doc *dom.Document) {
	root := doc.DocumentElement()
	items := root.GetElementsByTagName("p")
	for lp := uint(0); lp < items.Length(); lp++ {
		item := items.Item(lp)
		text := item.(*dom.Element).ToText(false)
		if strings.TrimSpace(string(text)) == "" {
			// check for special case where there is an image
			if item.(*dom.Element).GetElementsByTagName("img").Length() == 0 {
				item.ParentNode().RemoveChild(item)
			}
		}
	}
}

func applyTrimOnText(t *dom.Text) {
	s := t.Data()
	s = strings.TrimSpace(s)
	t.SetData(s)
}

func applyTrimOnTextLeft(t *dom.Text) {
	s := t.Data()
	s = strings.TrimLeftFunc(s, unicode.IsSpace)
	t.SetData(s)
}

func applyTrimOnTextRight(t *dom.Text) {
	s := t.Data()
	s = strings.TrimRightFunc(s, unicode.IsSpace)
	t.SetData(s)
}

func applyTrimOnLeft(n dom.Node) {
	switch n.NodeType() {

	case dom.TEXT_NODE:
		applyTrimOnTextLeft(n.(*dom.Text))

	case dom.ELEMENT_NODE:
		if n.ChildNodes().Length() > 0 {
			applyTrimOnLeft(n.ChildNodes().Item(0))
		}
	}
}

func applyTrimOnRight(n dom.Node) {
	switch n.NodeType() {

	case dom.TEXT_NODE:
		applyTrimOnTextRight(n.(*dom.Text))

	case dom.ELEMENT_NODE:
		if n.ChildNodes().Length() > 0 {
			applyTrimOnRight(n.ChildNodes().Item(0))
		}
	}
}

func applyTrimOnElem(e *dom.Element) {
	if e.ChildNodes().Length() == 0 {
		return
	}

	if e.ChildNodes().Length() == 1 {
		item := e.ChildNodes().Item(0)
		switch item.NodeType() {

		case dom.TEXT_NODE:
			applyTrimOnText(item.(*dom.Text))

		case dom.ELEMENT_NODE:
			applyTrimOnElem(item.(*dom.Element))

		}

		return
	}

	left := e.ChildNodes().Item(0)
	applyTrimOnLeft(left)
	right := e.ChildNodes().Item(e.ChildNodes().Length() - 1)
	applyTrimOnRight(right)
}

func applyTrimOnElems(doc dom.NodeList) {
	for lp := uint(0); lp < doc.Length(); lp++ {
		item := doc.Item(lp)
		applyTrimOnElem(item.(*dom.Element))
	}
}

func applyTrim(doc *dom.Document) {
	e := doc.DocumentElement()
	applyTrimOnElems(e.GetElementsByTagName("p"))
	applyTrimOnElems(e.GetElementsByTagName("h1"))
	applyTrimOnElems(e.GetElementsByTagName("h2"))
	applyTrimOnElems(e.GetElementsByTagName("h3"))
}

func isSpanEmpty(e *dom.Element) bool {
	cn := e.ChildNodes()
	length := cn.Length()

	if length == 0 {
		return true
	}
	text := e.ToText(false)
	return len(text) == 0
}

func applyRemoveEmptySpan(doc *dom.Document) {
	e := doc.DocumentElement()
	items := e.GetElementsByTagName("span")
	for lp := items.Length(); lp > 0; lp-- {
		i := items.Item(lp - 1)
		b := isSpanEmpty(i.(*dom.Element))
		if b {
			i.ParentNode().RemoveChild(i)
		}
	}
}

func ApplyFilters(doc *dom.Document, filters_str string) {
	filters_str = strings.TrimSpace(filters_str)
	if len(filters_str) == 0 {
		return
	}

	filters := strings.Split(filters_str, ",")

	for _, filter := range filters {
		fmt.Printf("\tFilter %s\n", filter)
		switch filter {
		case "hoist":
			applyHoist(doc)
		case "removeemptyspan":
			applyRemoveEmptySpan(doc)
		case "mdash":
			applyMDash(doc)
		case "replacehr":
			applyReplaceHr(doc)
		case "stripemptypara":
			applyStripEmptyPara(doc)
		case "smartquotes":
			applySmartQuotes(doc)
		case "trim":
			applyTrim(doc)
		default:
			panic("Unknown filter name.")
		}
	}
}
