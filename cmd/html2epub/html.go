package main

import (
	dom "bitbucket.org/rj/xmldom-go"
	"strings"
)

type ErrBadFormat string

func (bf ErrBadFormat) Error() string {
	return "Bad format: " + string(bf)
}

func GetHead(doc *dom.Document) (*dom.Element, error) {
	elem := doc.DocumentElement()
	if elem.NodeName() != "html" {
		return nil, ErrBadFormat("Root element is not HTML")
	}
	if !elem.HasChildNodes() {
		return nil, ErrBadFormat("Root element does not contain any elements")
	}

	// Find head
	head := FirstElement(elem.ChildNodes())
	if head.NodeName() != "head" {
		return nil, ErrBadFormat("First element under root is not 'head'")
	}
	return head, nil
}

func GetBody(doc *dom.Document) (*dom.Element, error) {
	elem := doc.DocumentElement()
	if elem.NodeName() != "html" {
		return nil, ErrBadFormat("Root element is not HTML")
	}
	if !elem.HasChildNodes() {
		return nil, ErrBadFormat("Root element does not contain any elements")
	}

	// Find body
	body := SecondElement(elem.ChildNodes())
	if body.NodeName() != "body" {
		return nil, ErrBadFormat("Second element under root is not 'body'")
	}
	return body, nil
}

func FirstElement(nl dom.NodeList) *dom.Element {
	for i := uint(0); i < nl.Length(); i++ {
		node := nl.Item(i)
		if node.NodeType() == dom.ELEMENT_NODE {
			return node.(*dom.Element)
		}
	}

	return nil
}

func SecondElement(nl dom.NodeList) *dom.Element {
	c := 0

	for i := uint(0); i < nl.Length(); i++ {
		node := nl.Item(i)
		if node.NodeType() == dom.ELEMENT_NODE {
			c++
			if c == 2 {
				return node.(*dom.Element)
			}
		}
	}

	return nil
}

func StripScriptElementsFromHead(doc *dom.Document) error {
	head, err := GetHead(doc)
	if err != nil {
		return err
	}
	children := head.ChildNodes()

	for lp := children.Length(); lp > 0; lp-- {
		child := children.Item(lp - 1)
		if child.NodeType() == dom.ELEMENT_NODE && child.NodeName() == "script" {
			head.RemoveChild(child)
			if lp-1 < children.Length() {
				child = children.Item(lp - 1)
				if child.NodeType() == dom.TEXT_NODE && child.NodeValue()[0:1] == "\n" {
					head.RemoveChild(child)
				}
			}
		}
	}

	return nil
}

func elementIsLinkToStyleSheet(node dom.Node) bool {
	if node.NodeType() != dom.ELEMENT_NODE || node.NodeName() != "link" {
		return false
	}

	elem := node.(*dom.Element)
	return elem.GetAttribute("rel") == "stylesheet"
}

func StripStylesheetElementsFromHead(doc *dom.Document) error {
	head, err := GetHead(doc)
	if err != nil {
		return err
	}
	children := head.ChildNodes()

	for lp := children.Length(); lp > 0; lp-- {
		child := children.Item(lp - 1)
		if elementIsLinkToStyleSheet(child) {
			head.RemoveChild(child)
			if lp-1 < children.Length() {
				child = children.Item(lp - 1)
				if child.NodeType() == dom.TEXT_NODE && child.NodeValue()[0:1] == "\n" {
					head.RemoveChild(child)
				}
			}
		}
	}

	return nil
}

func StripLeadingWhitespace(doc *dom.Document) {
	bodyelems := doc.DocumentElement().GetElementsByTagName("body")
	if bodyelems.Length() != 1 {
		panic("Improperly formatted HTML document.  Not appropriate count of body elements.")
	}

	body := bodyelems.Item(0).(*dom.Element)
	children := body.ChildNodes()

	for children.Item(0).NodeType() == dom.TEXT_NODE && strings.TrimSpace(children.Item(0).NodeValue()) == "" {
		body.RemoveChild(children.Item(0))
	}
}
