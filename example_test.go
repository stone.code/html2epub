package epub

import (
	"bytes"
	"fmt"
	"os"
	"time"
)

func createChapaters(opf *OPF, ncx *NCX, writer *Writer, doc string) error {
	// This is just a mock for the example.
	return nil
}

func ExampleHello() {
	const outfilename = "./test.epub"

	dc := DublinCore{
		Title:      "Example EPUB",
		Creator:    "John and Jane Doe",
		Date:       time.Now(),
		Language:   "en",
		Identifier: "none",
	}

	// Create a file for the EPUB
	file, err := os.OpenFile(outfilename, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer file.Close()
	defer os.Remove(outfilename)

	// Initialize the zip writer for the EPUB.  Note, the zip writer from the
	// standard library isn't used because with need the zip's directory to be
	// located at the end of the file
	writer := NewWriter(file)
	defer writer.Close()

	// The very first file added to the EPUB must be the mimetype file.
	err = CreateMimeTypeFile(writer)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	// Add the required file META-INF/container.xml
	err = CreateMetaInf(writer)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	// All other files need to be recorded by OPF, so create that structure now.
	// Pass in the metadata for the book.
	opf := NewOPFWithDublinCore(&dc)
	// Not required, but we add a makefile to the EPUB.  The book can then be
	// unzipped into a directory, modified, and then the makefile can be used
	// to rebuild the book.
	err = CreateMakefile(writer, &opf, outfilename)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	// Next, we will be building the content.  This needs to be recorded in the NCX
	// to create navigation.
	ncx := NewNCX()
	// Add a title page to the book
	CreateTitlePage(writer, &opf, &ncx)
	// Add chapters to the book.  The following function is a mock.  A real implementation
	// would obviously need to provide some content.
	CreateSection(writer, &opf, &ncx, "chapter1", "chapter1.xhtml", "Section Title", []byte("<html><body><p>My book contents...</p></body></html>"))
	// Write the NCX to the book.
	err = ncx.Write(writer, &opf)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	// Write the OPF to the book.  This must be the last file added.
	err = opf.Write(writer)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	// Print out some text.  Not really useful for the example, but it forces the
	// example to be run during testing.
	fmt.Printf("Manifest: %d item(s)\nSpine: %d items(s)\nGuide: %d item(s)\n",
		opf.GetManifestCount(), opf.GetSpineCount(), opf.GetGuideCount())
	fmt.Println("Done.")

	// Output:
	// Manifest: 4 item(s)
	// Spine: 2 items(s)
	// Guide: 2 item(s)
	// Done.
}

type nopLoader struct{}

func (*nopLoader) Load(url string) ([]byte, error) {
	return []byte{}, nil
}

func ExampleCreateCover() {
	const outfilename = "./test.epub"

	// We need to establish some setup before we can call CreateCover.  Much will
	// be elided.  See the full-fledge example for a complete setup for a valid
	// ePub.
	dc := DublinCore{
		Title:         "Example EPUB",
		Creator:       "John and Jane Doe",
		Date:          time.Now(),
		Language:      "en",
		Identifier:    "none",
		CoverURL:      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/PNG_transparency_demonstration_1.png/280px-PNG_transparency_demonstration_1.png",
		CoverMimeType: "image/png",
	}

	// Create an in-memory file for the EPUB
	file := bytes.NewBuffer(nil)
	writer := NewWriter(file)
	defer writer.Close()
	// Important files required by the standard skipped.
	// ...

	// Create the OPF and NCX structures
	opf := NewOPFWithDublinCore(&dc)
	ncx := NewNCX()

	// CreateCover
	CreateCover(writer, &opf, &ncx, &nopLoader{})
	// Write the NCX to the book.
	_ = ncx.Write(writer, &opf)
	// Write the OPF to the book.  This must be the last file added.
	_ = opf.Write(writer)

	// Print out some text.  Not really useful for the example, but it forces the
	// example to be run during testing.
	fmt.Println("Done.")

	// Output:
	// Done.
}

func ExampleOPF_AddItemWithFallback() {
	// We need to establish some setup before we can call CreateCover.  Much will
	// be elided.  See the full-fledge example for a complete setup for a valid
	// ePub.
	dc := DublinCore{
		Title:         "Example EPUB",
		Creator:       "John and Jane Doe",
		Date:          time.Now(),
		Language:      "en",
		Identifier:    "none",
		CoverURL:      "https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/PNG_transparency_demonstration_1.png/280px-PNG_transparency_demonstration_1.png",
		CoverMimeType: "image/png",
	}

	// Create an in-memory file for the EPUB
	file := bytes.NewBuffer(nil)
	writer := NewWriter(file)
	defer writer.Close()
	// Important files required by the standard skipped.
	// ...

	// Create the OPF and NCX structures
	opf := NewOPFWithDublinCore(&dc)
	ncx := NewNCX()

	// Compress and store an SVG image, which could be used within the text.
	_ = writer.DeflateEntry("image.svg", []byte("<svg></svg>"))
	opf.AddItem("image-svg", "image.svg", "")
	// What if the reader does not support SVG?  Provide a PNG fallback
	_ = writer.StoreEntry("image.png", []byte{'P', 'N', 'G', 0})
	opf.AddItemWithFallback("image-png", "image.png", "", "image-svg")

	// CreateCover
	CreateCover(writer, &opf, &ncx, &nopLoader{})
	// Write the NCX to the book.
	_ = ncx.Write(writer, &opf)
	// Write the OPF to the book.  This must be the last file added.
	_ = opf.Write(writer)

	// Print out some text.  Not really useful for the example, but it forces the
	// example to be run during testing.
	fmt.Println("Done.")

	// Output:
	// Done.
}
