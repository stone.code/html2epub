package epub

import (
	"testing"
)

func TestIsValidISBN10(t *testing.T) {
	cases := []struct {
		in  string
		out bool
	}{
		{"817525766-0", true},
		{"817525766-1", false},
		{"817525766-9", false},
		{"8175-25766-0", true},
		{"8175-25766-1", false},
		{"8175-25766-9", false},
		{"8175257660", true},
		{"8175257661", false},
		{"8175257669", false},
		{"817525766a", false},   // bad character
		{"817525766-01", false}, // too long
		{"817525766", false},    // too short
	}

	for i, v := range cases {
		out := IsValidISBN10(v.in)
		if v.out != out {
			t.Errorf("%d: got %d, expected %d", i, out, v.out)
		}

		out = IsValidISBN(v.in)
		if v.out != out {
			t.Errorf("%d: got %d, expected %d", i, out, v.out)
		}
	}
}

func TestIsValidISBN13(t *testing.T) {
	cases := []struct {
		in  string
		out bool
	}{
		{"978-1-4757-1077-9", true},
		{"978-1-4757-1077-8", false},
		{"978-1-4020-8021-0", true},
		{"978-1-4020-8021-1", false},
		{"978-1-4020-8021-9", false},
		{"978-1-4020-8021-a", false},  // bad character
		{"978-1-4020-8021-91", false}, // Too long
		{"978-1-4020-802-9", false},   // Too short
	}

	for i, v := range cases {
		out := IsValidISBN13(v.in)
		if v.out != out {
			t.Errorf("%d: got %d, expected %d", i, out, v.out)
		}

		out = IsValidISBN(v.in)
		if v.out != out {
			t.Errorf("%d: got %d, expected %d", i, out, v.out)
		}
	}
}
